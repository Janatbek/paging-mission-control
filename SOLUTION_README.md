# Paging Mission Control Solution
## How to run:
> create a txt file and save it in solution folder. remember the name of the file, without extension. project doesn't have any npm dependencies, just one solution.js file. 

### In the terminal run
```
node solution.js [file name]
```
replace [file name] with the txt file name. 

### Example for this solution. 
```
node solution.js input
```



