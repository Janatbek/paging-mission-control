// get filesystem module
const fs = require("fs");
// arguments to scripts. usually first two are node and path to file. 
const [fileName] = process.argv.slice(2); //pass file name to program. 

// using the readFileSync() function
// and passing the path to the file

const buffer = fs.readFileSync(`${fileName}.txt`);

// use the toString() method to convert
// Buffer into String
const fileContent = buffer.toString();
// I suppose I had to use regex and extract kebab-cased values as camelCase?!
// I just hard coded values in a class, so this variale is not used.
const format = `<timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>`;
// Called a class measure since it has measurments. 
class Measure {
  constructor(measure) {
    const [timestamp, satelliteId, redHighLimit, yellowHighLimit, yellowLowLimit, redLowLimit, rawValue, component] = measure;
    this.timestamp = timestamp;
    this.satelliteId = satelliteId;
    this.redHighLimit = redHighLimit;
    this.yellowHighLimit = yellowHighLimit;
    this.yellowLowLimit = yellowLowLimit;
    this.redLowLimit = redLowLimit;
    this.rawValue = rawValue;
    this.component = component;
  }
}
// this class monitors data. 
class Monitor {
  data = {};

  add(measure) {
      // create map of data by satelliteId and component. 
    if(!this.data.hasOwnProperty(measure.satelliteId)) {
      this.data[measure.satelliteId] = {};
    }
    if(!this.data[measure.satelliteId].hasOwnProperty(measure.component)){
      this.data[measure.satelliteId][measure.component] = [];
    };

    if(measure.component === 'BATT' && +measure.rawValue < +measure.redLowLimit) {
      this.data[measure.satelliteId][measure.component].push({
        satelliteId: measure.satelliteId,
        severity: 'RED LOW',
        component: measure.component,
        timestamp: measure.timestamp
      });
    } else if(measure.component === 'TSTAT' && +measure.rawValue > +measure.redHighLimit) {
      this.data[measure.satelliteId][measure.component].push({
        satelliteId: measure.satelliteId,
        severity: 'RED HIGH',
        component: measure.component,
        timestamp: measure.timestamp
      });
    }
  }

  alert() {
    const result = [];
    // loop through satelliteIds: complexity? I don't think can be too much satelites
    Object.keys(this.data).forEach(satelliteIdKey => {
        // loop through components: BATT, and TSTAT. 
      Object.keys(this.data[satelliteIdKey]).forEach(componentKey => {
          // if we have three or more readings...
        if(this.data[satelliteIdKey][componentKey].length >= 3 ) {
            // we need to find out if those 3 are in 5 minute range.
          let fiveMinuteInterval;
          const tempFilter = this.data[satelliteIdKey][componentKey].filter((alert, i) => {
            const time = alert.timestamp.split(' ')[1].split(':');
            const minutes = time[1];
            if(i === 0) {
              fiveMinuteInterval = +minutes + 5;
            }
            return minutes < fiveMinuteInterval;
          });
          if(tempFilter.length >=3) {
            result.push (tempFilter[0]);
          }
        }
      })
    });
    console.log(JSON.stringify(result))
  }
}

const monitor = new Monitor();
fileContent.split('\n').forEach(item => {
  monitor.add(new Measure(item.split('|')))
});

monitor.alert();
// I could add interval.